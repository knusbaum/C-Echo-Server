#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <memory.h>
#include <arpa/inet.h>
#include <sys/select.h>
#include <sys/socket.h>
#include "linked_fd.h"

int highest_fd = 0;

void close_all(linked_fd *fdlist);
void accept_sock(int listen_sock, linked_fd *fdlist);
void do_echo(int fd, linked_fd *fdlist);

int main(void) {

    struct sockaddr_in serv_addr;
    int listen_sock = socket(AF_INET, SOCK_STREAM, 0);
    linked_fd * fdlist = new_linked_fd();
    
    memset(&serv_addr, 0, sizeof(serv_addr));
    serv_addr.sin_family=AF_INET;
    serv_addr.sin_addr.s_addr=htonl(INADDR_ANY);
    serv_addr.sin_port=htons(5058);
    
    int bind_result = bind(listen_sock,
                           (struct sockaddr*)&serv_addr, sizeof(serv_addr));
    if(bind_result == -1) {
        perror("Failed to bind.");
        return EXIT_FAILURE;
    }
    
    listen(listen_sock, 100);

    fd_set fdset;
    highest_fd = listen_sock;

    do {
        FD_ZERO(&fdset);
        // Add the listening socket to the fdset.
        FD_SET(listen_sock, &fdset);
        // Add stdin to the fileset.
        FD_SET(STDIN_FILENO, &fdset);

        // Add each fd in our fdlist to the set.
        linked_fd_node *current = linked_fd_head(fdlist);
        while(current) {
            int fd = fd_at(current);
            if(fd < 0) {
                return EXIT_FAILURE;
            }
            FD_SET(fd, &fdset);
            current = linked_fd_next(current);
        }

        // Select on every fd in the fdset.
        int x = select(highest_fd + 1, &fdset, NULL, NULL, NULL);
        if(x < 0) {
            return EXIT_FAILURE;
        }

        // If we got something on stdin, close all sockets
        // And exit.
        if(FD_ISSET(STDIN_FILENO, &fdset)) {
            printf("Closing all sockets.\n");
            close(listen_sock);
            close_all(fdlist);
        }

        // If the listen_sock is 'ready', accept a socket and
        // add it to our fd list.
        if(FD_ISSET(listen_sock, &fdset)) {
            accept_sock(listen_sock, fdlist);
        }

        //Handle other fds.
        current = linked_fd_head(fdlist);
        while(current) {
            int fd = fd_at(current);
            // This should never happen. Just precautionary.
            if(fd < 0) {
                return EXIT_FAILURE;
            }
            if(FD_ISSET(fd, &fdset)) {
                do_echo(fd, fdlist);
            }
            current = linked_fd_next(current);
        }
        
    } while(1);
}

void close_all(linked_fd *fdlist) {
    linked_fd_node *each = linked_fd_head(fdlist);
    while(each) {
        int fd = fd_at(each);
        if(fd > 0) {
            printf("Closing: %d\n", fd);
            close(fd);
        }
        each = linked_fd_next(each);
    }
}

void accept_sock(int listen_sock, linked_fd *fdlist) {
    int newsock = accept(listen_sock, NULL, NULL);
    printf("Accepted sock.\n");
    //newsock > 0, no error. We have a real connection.
    if(newsock > 0) {
        linked_fd_push(newsock, fdlist);
        // If newsock > highest_fd, we need to reset highest_fd
        if(newsock > highest_fd) {
            highest_fd = newsock;
        }
    }
    else {
        // Otherwise, print an error and exit.
        perror("Failed to accept");
        exit(EXIT_FAILURE);
    }
}

void do_echo(int fd, linked_fd *fdlist) {
    char buffer[10];
    int read_count = read(fd, buffer, 10);
    // FD got closed.
    if(read_count == 0) {
        printf("Closing FD: %d\n", fd);
        linked_fd_remove(fd, fdlist);
    }
    // Read from the FD
    else {
        int j;
        for(j = 0; j < read_count; j++) {
            printf("%c", buffer[j]);
        }
        printf("\n");
        
        // Write to all the buffers.
        linked_fd_node * each = linked_fd_head(fdlist);
        while(each) {
            int fd = fd_at(each);
            if(fd > 0) {
                write(fd, buffer, read_count);
            }
            each = linked_fd_next(each);
        }
    }
}
