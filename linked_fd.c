#include "linked_fd.h"
#include <stdlib.h>
#include <string.h>

struct linked_fd_node {
    linked_fd_node *next;
    int fd;
};

struct linked_fd {
    linked_fd_node *head;
};

linked_fd *new_linked_fd() {
    linked_fd *new = malloc(sizeof(linked_fd));
    if(!new) {
        return NULL;
    }
    memset(new, 0, sizeof(linked_fd));
    return new;
}

void destroy_linked_fd(linked_fd * fdlist) {
    linked_fd_node *current = fdlist->head;
    while(current) {
        linked_fd_node *next = current->next;
        free(current);
        current = next;
    }
    free(fdlist);
}

int linked_fd_push(int fd, linked_fd *fdlist) {
    if(fdlist == NULL) {
        return 0;
    }
    linked_fd_node *newnode = malloc(sizeof(linked_fd_node));
    if(!newnode) {
        return 0;
    }
    newnode->fd = fd;
    newnode->next = fdlist->head;
    fdlist->head = newnode;

    return 1;
}

void linked_fd_remove(int fd, linked_fd *fdlist) {
    if(fdlist){
        linked_fd_node *current = fdlist->head;
        if(current && current->fd == fd) {
            fdlist->head = current->next;
            free(current);
            return;
        }
        
        while(current) {
            linked_fd_node *next = current->next;
            if(next && next->fd == fd) {
                current->next = next->next;
                free(next);
            }
            current = current->next;
        }
    }
}

linked_fd_node *linked_fd_head(linked_fd *fdlist) {
    if(fdlist) {
        return fdlist->head;
    }
    return NULL;
}

linked_fd_node *linked_fd_next(linked_fd_node *node) {
    if(node) {
        return node->next;
    }
    return NULL;
}

int fd_at(linked_fd_node *node) {
    if(node) {
        return node->fd;
    }
    return -1;
}
