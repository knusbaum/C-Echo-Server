#include "linked_fd.h"
#include <stdlib.h>
#include <stdio.h>

void print_list(linked_fd *fdlist) {
    
    linked_fd_node * current = linked_fd_head(fdlist);
    if(!current) {
        return;
    }
    while(current) {
        printf("Current fd: %d\n", fd_at(current));
        current = linked_fd_next(current);
    }
}

int main(void) {
    linked_fd * list = NULL;
    list = new_linked_fd();
    if(!list) {
        return EXIT_FAILURE;
    }
       
    int i = 0;
    for(i = 0; i < 10; i++) {
        int ok = linked_fd_push(i, list);
        if(!ok) {
            printf("Failed to push fd %d\n", i);
            exit(EXIT_FAILURE);
        }
    }
    
    print_list(list);
    
    for(i = 0; i < 10; i+=2) {
        printf("Removing %d\n", i);
        linked_fd_remove(i, list);
    }
    printf("___________________\n");
    print_list(list);
    
    destroy_linked_fd(list);
    
    return EXIT_SUCCESS;
}

