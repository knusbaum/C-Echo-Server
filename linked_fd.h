#ifndef LINKED_FD_H
#define LINKED_FD_H

typedef struct linked_fd linked_fd;
typedef struct linked_fd_node linked_fd_node;

linked_fd * new_linked_fd();
void destroy_linked_fd(linked_fd *fdlist);

// Returns 1 for sucess,
// Returns 0 for failure.
int linked_fd_push(int fd, linked_fd *fdlist);
void linked_fd_remove(int fd, linked_fd *fdlist);
linked_fd_node *linked_fd_head(linked_fd *fdlist);
linked_fd_node *linked_fd_next(linked_fd_node *node);
int fd_at(linked_fd_node *node);


#endif
